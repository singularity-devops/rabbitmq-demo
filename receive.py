import pika
credentials = pika.PlainCredentials('guest', 'guest')
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost',5672,'/', credentials))
channel = connection.channel()

method_frame, header_frame, body = channel.basic_get('hello')
print(method_frame, header_frame, body)
channel.basic_ack(method_frame.delivery_tag)

channel.start_consuming()